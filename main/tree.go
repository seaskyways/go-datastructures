package main

import (
	"bufio"
	"fmt"
	. "go-datastructures/tree"
	"os"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("Started tree test")

	numbersToAdd := make([]int, 0)

	printHelp()
	var root *BinaryTreeNode

	scanner := bufio.NewScanner(os.Stdin)

	for {
		func() {
			defer func() {
				if msg := recover(); msg != nil {
					fmt.Println(msg.(string))
				}
			}()

			fmt.Print(">> ")

			if !scanner.Scan() {
				return
			}

			cmd := scanner.Text()

			cmd = strings.TrimSpace(cmd)
			args := strings.Split(cmd, " ")

			if len(args) == 0 {
				return
			}

			switch args[0] {
			case "help":
				printHelp()

			case "test":
				runTest(numbersToAdd)

			case "balance":
				root = BalanceNode(root)

			case "printbalance":
				printBalanceFactors(root)

			case "height":
				fmt.Println(root.Height())

			case "delete":
				if len(args) < 2 {
					panic("Delete requires a second argument")
				}
				key, err := strconv.ParseInt(args[1], 10, 64)
				if err != nil {
					panic("Delete requires an integer as 2nd argument")
				}
				root = Delete(root, int(key))

			case "print":
				break

			default:
				key, err := strconv.ParseInt(args[0], 10, 64)
				if err != nil {
					panic("Invalid command")
				}
				root = InsertRecursive(root, int(key))
			}

			PrintTreeHorizontal(root)

		}()
	}

}

func printHelp() (int, error) {
	return fmt.Print(`
How to use:

1- Enter a number to add to the tree

2- Enter one of the following for commands:
	- test 			: run an automated test
	- print			: prints the tree from root node
	- balance 		: balances tree from root node
	- printbalance	: prints balance factor at every node
	- height 		: prints height of tree from root
	- delete {key}	: searches for {key} and deletes it if exists
`)
}

func runTest(numbersToAdd []int) {
	numbersToAdd = append(numbersToAdd, 1, 2, 3, 4, 5, 6, 7, 8, 9)
	fmt.Println("Inserting numbers:", numbersToAdd)
	var root *BinaryTreeNode = nil
	// for each number in numbersToAdd
	for _, number := range numbersToAdd {
		root = InsertRecursive(root, number)
	}
	fmt.Println("Starting Tree structure:")
	PrintTreeHorizontal(root)
	fmt.Println("Attempt balance tree")
	root = BalanceNode(root)
	fmt.Println("After balancing:")
	PrintTreeHorizontal(root)
	printBalanceFactors(root)
}

func printBalanceFactors(root *BinaryTreeNode) {
	root.ForEach(func(node *BinaryTreeNode) {
		fmt.Println("Key =", node.Key, "BalanceFactor =", node.BalanceFactor())
	})
}

func PrintDashLine() {
	fmt.Println(strings.Repeat("―", 30))
}

/*
Prints the tree nodes in a tree like structure
*/
func PrintTreeHorizontal(node *BinaryTreeNode) {

	var recur func(node *BinaryTreeNode, level int, prefix string)

	recur = func(node *BinaryTreeNode, level int, prefix string) {
		if node == nil {
			return
		}
		recur(node.Left, level+2, strconv.Itoa(node.Key)+"L")

		dashes := strings.Repeat(" ", level)
		dashes += prefix
		dashes += "> "

		fmt.Printf("%s%d\n", dashes, node.Key)
		recur(node.Right, level+2, strconv.Itoa(node.Key)+"R")
	}

	PrintDashLine()
	recur(node, 1, ``)
	PrintDashLine()

}
