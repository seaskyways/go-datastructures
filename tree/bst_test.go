package tree

import (
	"testing"
)

func TestBinaryTree_Insert(t *testing.T) {
	tree := NewBinaryTree()
	tree.Insert(5)

	tree.Insert(3)

	tree.Insert(2)

	tree.Insert(1)

	PrintTreeNodePadded(tree.root, 1, t)
}

func TestBinaryTreeNode_InsertRecursive(t *testing.T) {

	var root *BinaryTreeNode = nil

	root = InsertRecursive(root, 6)
	InsertRecursive(root, 5)
	InsertRecursive(root, 1)
	InsertRecursive(root, 2)
	InsertRecursive(root, 8)

	PrintTreeNodePadded(root, 1, t)
}

func TestCountRecursive(t *testing.T) {
	root := GenExampleTree()

	PrintTreeNodeOrdered(root, t)

	t.Logf("Count of nodes in tree : %d", root.CountRecursive())
}

func TestCountLeaveNodes(t *testing.T) {
	root := GenExampleTree()

	t.Logf("Count of leaf nodes in tree : %d", CountLeaveNodes(root))
}

func TestBinaryTreeNode_Height(t *testing.T) {
	root := GenExampleTree()

	t.Logf("Height of tree : %d", root.Height())
}

func TestBinaryTreeNode_BalanceFactor(t *testing.T) {
	root := GenExampleTree()

	t.Logf("Balance factor of root : %d", root.BalanceFactor())
}

func GenExampleTree() *BinaryTreeNode {
	var root *BinaryTreeNode = nil
	root = InsertRecursive(root, 6)
	root = InsertRecursive(root, 5)
	root = InsertRecursive(root, 2)
	root = InsertRecursive(root, 8)
	root = InsertRecursive(root, 9)
	root = InsertRecursive(root, 3)
	root = InsertRecursive(root, 1)
	root = InsertRecursive(root, 10)
	return root
}

func PrintTreeNodeOrdered(node *BinaryTreeNode, t *testing.T) {
	if node == nil {
		return
	}
	PrintTreeNodeOrdered(node.Left, t)
	t.Logf("%d", node.Key)
	PrintTreeNodeOrdered(node.Right, t)
}

//Prints the tree nodes in a tree like structure
func PrintTreeNodePadded(node *BinaryTreeNode, level int, t *testing.T) {
	if node == nil {
		return
	}
	PrintTreeNodePadded(node.Left, level+1, t)

	dashes := ""
	for i := 0; i < level; i++ {
		dashes += "-"
	}

	t.Logf("%s%d", dashes, node.Key)
	PrintTreeNodePadded(node.Right, level+1, t)
}

/*
Prints the tree nodes in a tree like structure
*/
func PrintTreeHorizontal(node *BinaryTreeNode, level int, t *testing.T) {
	if node == nil {
		return
	}
	PrintTreeHorizontal(node.Left, level+2, t)

	dashes := ""
	for i := 1; i < level; i++ {
		dashes += " "
	}
	dashes += "> "

	t.Logf("%s%d", dashes, node.Key)
	PrintTreeHorizontal(node.Right, level+2, t)
}

func PrintTreeBalanceFactors(node *BinaryTreeNode, t *testing.T) {
	if node == nil {
		return
	}

	PrintTreeBalanceFactors(node.Left, t)
	t.Logf("%d : %d", node.Key, node.BalanceFactor())
	PrintTreeBalanceFactors(node.Right, t)
}

func TestLeftRotate(t *testing.T) {
	root := GenExampleTree()
	InsertRecursive(root, 0)

	t.Log("Preformatting : ")
	PrintTreeHorizontal(root, 1, t)

	root.Left.Left = LeftRotate(root.Left.Left)

	t.Log("Post-formatting : ")
	PrintTreeHorizontal(root, 1, t)
}

func TestBalanceNode(t *testing.T) {
	var root *BinaryTreeNode = nil

	root = InsertRecursive(root, 50)
	InsertRecursive(root, 72)
	InsertRecursive(root, 17)
	InsertRecursive(root, 12)
	InsertRecursive(root, 9)
	InsertRecursive(root, 14)
	InsertRecursive(root, 23)
	InsertRecursive(root, 19)
	InsertRecursive(root, 54)
	InsertRecursive(root, 76)
	InsertRecursive(root, 67)

	// till now it is balanced
	// let's add 20 which will unbalance it
	// 20 should be added under 19 Right
	InsertRecursive(root, 20)

	PrintTreeBalanceFactors(root, t)
	PrintTreeHorizontal(root, 1, t)

	root = BalanceNode(root)

	t.Log("After balancing ")

	PrintTreeBalanceFactors(root, t)
	PrintTreeHorizontal(root, 1, t)
}
