package tree

type BinaryTreeNode struct {
	Key   int
	Left  *BinaryTreeNode
	Right *BinaryTreeNode
}

type BinaryTree struct {
	root *BinaryTreeNode
}

func NewBinaryTree() BinaryTree {
	tree := BinaryTree{
		root: nil,
	}
	return tree
}

func (tree *BinaryTree) Insert(value int) (ok bool) {
	if tree.root == nil {
		tree.root = &BinaryTreeNode{Key: value}
	} else {
		current := tree.root
		for {
			switch {
			case value < current.Key:
				if current.Left == nil {
					current.Left = &BinaryTreeNode{
						Key: value,
					}
					return true
				} else {
					current = current.Left
				}

			case value > current.Key:
				if current.Right == nil {
					current.Right = &BinaryTreeNode{Key: value}
					return true
				} else {
					current = current.Right
				}
			}
		}
	}

	return false
}

func InsertRecursive(node *BinaryTreeNode, value int) *BinaryTreeNode {
	if node == nil {
		node = &BinaryTreeNode{Key: value}
	} else {
		if value < node.Key {
			node.Left = InsertRecursive(node.Left, value)
		} else if value > (*node).Key {
			node.Right = InsertRecursive(node.Right, value)
		}
	}
	return node
}

func (root *BinaryTreeNode) CountRecursive() int {
	if root == nil {
		return 0
	} else {
		return 1 + root.Left.CountRecursive() + root.Right.CountRecursive()
	}
}

func (root *BinaryTreeNode) Height() int {
	if root == nil {
		return 0
	}
	if root.Left == nil && root.Right == nil {
		return 1
	}

	leftHeight := root.Left.Height()
	rightHeight := root.Right.Height()

	if leftHeight > rightHeight {
		return 1 + root.Left.Height()
	} else {
		return 1 + root.Right.Height()
	}
}

func CountLeaveNodes(node *BinaryTreeNode) int {
	if node == nil {
		return 0
	}

	if node.Left == nil && node.Right == nil {
		return 1
	}

	return CountLeaveNodes(node.Left) + CountLeaveNodes(node.Right)
}

func (root *BinaryTreeNode) BalanceFactor() int {
	if root == nil {
		return 0
	}
	return root.Left.Height() - root.Right.Height()
}

func LeftRotate(root *BinaryTreeNode) *BinaryTreeNode {
	a := root
	b := a.Left

	a.Left = b.Right
	b.Right = a

	return b
}

func LeftRightRotate(root *BinaryTreeNode) *BinaryTreeNode {
	a := root
	b := a.Left
	c := b.Right

	a.Left = c.Right
	b.Right = c.Left
	c.Left = b
	c.Right = a

	return c
}

func RightRotate(root *BinaryTreeNode) *BinaryTreeNode {
	a := root
	b := a.Right

	a.Right = b.Left
	b.Left = a

	return b
}

func RightLeftRotate(root *BinaryTreeNode) *BinaryTreeNode {
	a := root
	b := a.Right
	c := b.Left

	a.Right = c.Left
	b.Left = c.Right
	c.Right = b
	c.Left = a

	return c
}

func BalanceNode(root *BinaryTreeNode) *BinaryTreeNode {
	if root == nil {
		return nil
	}
	root.Left = BalanceNode(root.Left)
	root.Right = BalanceNode(root.Right)

	balanceFactor := root.BalanceFactor()

	if balanceFactor >= 2 {
		if root.Left.BalanceFactor() <= -1 {
			root = LeftRightRotate(root)
		} else {
			root = LeftRotate(root)
		}
	} else if balanceFactor <= -2 {
		if root.Right.BalanceFactor() >= 1 {
			root = RightLeftRotate(root)
		} else {
			root = RightRotate(root)
		}
	}

	return root
}

func InsertAndBalance(root *BinaryTreeNode, key int) *BinaryTreeNode {
	node := InsertRecursive(root, key)
	node = BalanceNode(root)
	return node
}

func DeleteAndBalance(root *BinaryTreeNode, key int) *BinaryTreeNode {
	if root == nil {
		return nil
	}

	if root.Key == key {
		return nil
	}

	root = Delete(root, key)
	root = BalanceNode(root)

	return root
}

func Delete(root *BinaryTreeNode, key int) *BinaryTreeNode {
	if root == nil {
		return nil
	}

	if root.Key == key {
		return nil
	}

	root.Left = Delete(root.Left, key)
	root.Right = Delete(root.Right, key)

	return root
}

func (root *BinaryTreeNode) ForEach(action func(node *BinaryTreeNode)) {
	if root == nil {
		return
	}
	root.Left.ForEach(action)
	action(root)
	root.Right.ForEach(action)
}
